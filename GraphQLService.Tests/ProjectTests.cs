using System;
using Xunit;
using Snapshooter.Xunit;
using GraphQLService.Api.Models;
using GraphQLService.Api.Projects;
using GraphQLService.Api.Types;
using HotChocolate;
using HotChocolate.Execution;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using GraphQLService.Api.Infrastructure.Implementations;

namespace GraphQLService.Tests
{
    public class ProjectTests
    {
        [Fact]
        public async void Project_Schema_Changed()
        {
            ISchema schema = await new ServiceCollection()
                .AddPooledDbContextFactory<ToDoDbContext>(
                    options => options.UseInMemoryDatabase("Data Source=conferences.db"))
                .AddGraphQL()
                .AddQueryType(d => d.Name("Query"))
                    .AddTypeExtension<ProjectQueries>()
                .AddMutationType(d => d.Name("Mutation"))
                    .AddTypeExtension<ProjectMutation>()
                .AddType<ProjectType>()
                .AddType<TaskType>()
                .AddType<EmployeeType>()
                .AddType<TimeAccountinType>()
                .AddType<TaskTypeType>()
                .AddGlobalObjectIdentification()
                .AddFiltering()
                .AddSorting()
                .BuildSchemaAsync();

            // assert
            schema.Print().MatchSnapshot();
        }
    }
}
