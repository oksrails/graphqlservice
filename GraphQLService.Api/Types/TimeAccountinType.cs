﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Types
{
    public class TimeAccountinType : ObjectType<TimeAccountingModel>
    {
        protected override void Configure(IObjectTypeDescriptor<TimeAccountingModel> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(t => t.Id)
                .ResolveNode((ctx, id) => ctx.DataLoader<TimeAccountingDataLoader>().LoadAsync(id, ctx.RequestAborted));

            descriptor
                .Field(t => t.Employee)
                .ResolveWith<TimeAccountingResolvers>(t => t.GetEmployee(default!, default!, default));

            descriptor
                .Field(t => t.Task)
                .ResolveWith<TimeAccountingResolvers>(t => t.GetTask(default!, default!, default));
        }

        private class TimeAccountingResolvers
        {
            public async Task<EmployeeModel> GetEmployee(
                [Parent] TimeAccountingModel timeAccounting,
                EmployeeByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                return await dataLoader.LoadAsync(timeAccounting.EmployeeId, cancellationToken);
            }

            public async Task<TaskModel> GetTask(
                [Parent] TimeAccountingModel timeAccounting,
                TaskByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                return await dataLoader.LoadAsync(timeAccounting.TaskId, cancellationToken);
            }
        }
    }
}
