﻿using GraphQLService.Api.Models;
using HotChocolate.Data.Filters;

namespace GraphQLService.Api.Types
{
    public class ProjectFilterInputType : FilterInputType<ProjectModel>
    {
        protected override void Configure(IFilterInputTypeDescriptor<ProjectModel> descriptor)
        {
            descriptor.Ignore(x => x.Id);
        }
    }
}
