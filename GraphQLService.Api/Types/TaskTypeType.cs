﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Types
{
    public class TaskTypeType : ObjectType<TaskTypeModel>
    {
        protected override void Configure(IObjectTypeDescriptor<TaskTypeModel> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(t => t.Id)
                .ResolveNode((ctx, id) => ctx.DataLoader<TaskTypeByIdDataLoader>().LoadAsync(id, ctx.RequestAborted));
        }
    }
}
