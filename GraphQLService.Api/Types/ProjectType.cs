﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Types
{
    public class ProjectType : ObjectType<ProjectModel>
    {
        protected override void Configure(IObjectTypeDescriptor<ProjectModel> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(t => t.Id)
                .ResolveNode((ctx, id) => ctx.DataLoader<ProjectByIdDataLoader>().LoadAsync(id, ctx.RequestAborted));

            descriptor
                .Field(t => t.ProjectEmployees)
                .ResolveWith<ProjectResolvers>(t => t.GetEmployeesAsync(default!, default!, default!, default))
                .UseDbContext<ToDoDbContext>()
                .UsePaging<NonNullType<EmployeeType>>()
                .Name("projectEmployees");
            
            descriptor
                .Field(t => t.Tasks)
                .ResolveWith<ProjectResolvers>(t => t.GetTasksAsync(default!, default!, default!, default))
                .UseDbContext<ToDoDbContext>()
                .UsePaging<NonNullType<TaskType>>()
                .Name("projectTasks");

            descriptor
                .Field(t => t.Owner)
                .ResolveWith<ProjectResolvers>(t => t.GetOwnerAsync(default!, default!, default))
                .Name("projectOwner");
        }

        private class ProjectResolvers
        {
            public async Task<IEnumerable<EmployeeModel>> GetEmployeesAsync(
                [Parent] ProjectModel project,
                [ScopedService] ToDoDbContext dbContext,
                EmployeeByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                Guid[] employeeIds = await dbContext.Projects
                    .Where(p => p.Id == project.Id)
                    .Include(p => p.ProjectEmployees)
                    .SelectMany(p => p.ProjectEmployees.Select(e => e.EmployeeId))
                    .ToArrayAsync();

                return await dataLoader.LoadAsync(employeeIds, cancellationToken);
            }
            
            public async Task<IEnumerable<TaskModel>> GetTasksAsync(
                [Parent] ProjectModel project,
                [ScopedService] ToDoDbContext dbContext,
                TaskByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                Guid[] taskIds = await dbContext.Projects
                    .Where(p => p.Id == project.Id)
                    .Include(p => p.Tasks)
                    .SelectMany(p => p.Tasks.Select(t => t.Id))
                    .ToArrayAsync();

                return await dataLoader.LoadAsync(taskIds, cancellationToken);
            }

            public async Task<EmployeeModel> GetOwnerAsync(
                [Parent] ProjectModel project,
                EmployeeByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                if (project.OwnerId is null)
                {
                    return null;
                }

                return await dataLoader.LoadAsync(project.OwnerId.Value, cancellationToken);
            }
        }
    }
}
