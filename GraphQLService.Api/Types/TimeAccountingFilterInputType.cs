﻿using GraphQLService.Api.Models;
using HotChocolate.Data.Filters;

namespace GraphQLService.Api.Types
{
    public class TimeAccountingFilterInputType : FilterInputType<TimeAccountingModel>
    {
        protected override void Configure(IFilterInputTypeDescriptor<TimeAccountingModel> descriptor)
        {
            descriptor.Ignore(x => x.Id);
        }
    }
}
