﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Models;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using HotChocolate;
using HotChocolate.Types;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace GraphQLService.Api.Types
{
    public class TaskType : ObjectType<TaskModel>
    {
        protected override void Configure(IObjectTypeDescriptor<TaskModel> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(t => t.Id)
                .ResolveNode((ctx, id) => ctx.DataLoader<TaskByIdDataLoader>().LoadAsync(id, ctx.RequestAborted));

            descriptor
                .Field(t => t.Assigned)
                .ResolveWith<TaskResolvers>(t => t.GetEmployeeAsync(default!, default!, default));

            descriptor
                .Field(t => t.TaskType)
                .ResolveWith<TaskResolvers>(t => t.GetTaskTypeAsync(default!, default!, default));

            descriptor
                .Field(t => t.Project)
                .ResolveWith<TaskResolvers>(t => t.GetProjectAsync(default!, default!, default));

            descriptor
                .Field(t => t.TimeAccountings)
                .ResolveWith<TaskResolvers>(t => t.GetTimeAccountingsAsync(default!, default!, default!, default))
                .UseDbContext<ToDoDbContext>()
                .UsePaging<NonNullType<TimeAccountinType>>();
        }

        private class TaskResolvers
        {
            public async Task<EmployeeModel?> GetEmployeeAsync(
                [Parent] TaskModel task,
                EmployeeByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                if (task.AssignedId is null)
                {
                    return null;
                }

                return await dataLoader.LoadAsync(task.AssignedId.Value, cancellationToken);
            }

            public async Task<ProjectModel?> GetProjectAsync(
                [Parent] TaskModel task,
                ProjectByIdDataLoader dataLoader,
                CancellationToken cancellation)
            {
                if (task.ProjectId is null)
                {
                    return null;
                }

                return await dataLoader.LoadAsync(task.ProjectId.Value, cancellation);
            }

            public async Task<TaskTypeModel?> GetTaskTypeAsync(
                [Parent] TaskModel task,
                TaskTypeByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                if (task.TaskTypeId is null)
                {
                    return null;
                }

                return await dataLoader.LoadAsync(task.TaskTypeId.Value, cancellationToken);
            }

            public async Task<IEnumerable<TimeAccountingModel>> GetTimeAccountingsAsync(
                [Parent] TaskModel task,
                [ScopedService] ToDoDbContext dbContext,
                TimeAccountingDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                Guid[] timeAccountingIds = await dbContext.Tasks
                    .Where(t => t.Id == task.Id)
                    .Include(t => t.TimeAccountings)
                    .SelectMany(t => t.TimeAccountings.Select(ta => ta.Id))
                    .ToArrayAsync();

                return await dataLoader.LoadAsync(timeAccountingIds, cancellationToken);
            }
        }
    }
}
