﻿using GraphQLService.Api.Models;
using HotChocolate.Data.Filters;

namespace GraphQLService.Api.Types
{
    public class TaskFilterInputType : FilterInputType<TaskModel>
    {
        protected override void Configure(IFilterInputTypeDescriptor<TaskModel> descriptor)
        {
            descriptor.Ignore(x => x.Id);
        }
    }
}
