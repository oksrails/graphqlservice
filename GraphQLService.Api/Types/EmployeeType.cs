﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Types
{
    public class EmployeeType : ObjectType<EmployeeModel>
    {
        protected override void Configure(IObjectTypeDescriptor<EmployeeModel> descriptor)
        {
            descriptor
                .ImplementsNode()
                .IdField(t => t.Id)
                .ResolveNode((ctx, id) => ctx.DataLoader<EmployeeByIdDataLoader>().LoadAsync(id, ctx.RequestAborted));
            
            descriptor
                .Field(t => t.ProjectEmployees)
                .ResolveWith<EmployeeResolvers>(t => t.GetProjectsAsync(default!, default!, default!, default))
                .UseDbContext<ToDoDbContext>()
                .UsePaging<NonNullType<ProjectType>>()
                .Name("projects");

            descriptor
                .Field(t => t.TimeAccountings)
                .ResolveWith<EmployeeResolvers>(t => t.GetEmployeeTimeAccounting(default!, default!, default!, default))
                .UseDbContext<ToDoDbContext>()
                .UsePaging<NonNullType<TimeAccountinType>>();
        }

        private class EmployeeResolvers
        {
            public async Task<IEnumerable<ProjectModel>> GetProjectsAsync(
                [Parent] EmployeeModel employee,
                [ScopedService] ToDoDbContext dbContext,
                ProjectByIdDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                Guid[] projectIds = await dbContext.Employees
                    .Where(e => e.Id == employee.Id)
                    .Include(e => e.ProjectEmployees)
                    .SelectMany(e => e.ProjectEmployees.Select(p => p.ProjectId))
                    .ToArrayAsync();

                return await dataLoader.LoadAsync(projectIds, cancellationToken);
            }

            public async Task<IEnumerable<TimeAccountingModel>> GetEmployeeTimeAccounting(
                [Parent] EmployeeModel employee,
                [ScopedService] ToDoDbContext dbContext,
                TimeAccountingDataLoader dataLoader,
                CancellationToken cancellationToken)
            {
                Guid[] timeAccountingIds = await dbContext.Employees
                    .Where(e => e.Id == employee.Id)
                    .Include(e => e.TimeAccountings)
                    .SelectMany(e => e.TimeAccountings.Select(t => t.Id))
                    .ToArrayAsync();

                return await dataLoader.LoadAsync(timeAccountingIds, cancellationToken);
            }
        }
    }
}
