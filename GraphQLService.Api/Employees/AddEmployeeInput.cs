﻿namespace GraphQLService.Api.Employees
{
    public record AddEmployeeInput(
        string FirstName,
        string LastName
    );
}
