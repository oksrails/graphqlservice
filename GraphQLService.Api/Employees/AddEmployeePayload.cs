﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.Employees
{
    public class AddEmployeePayload : EmployeePayloadBase
    {
        public AddEmployeePayload(EmployeeModel employee)
            : base(employee)
        { }

        public AddEmployeePayload(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
