﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.Employees
{
    public class EmployeePayloadBase : Payload
    {
        public EmployeeModel Employee { get; }

        protected EmployeePayloadBase(EmployeeModel employee)
        {
            Employee = employee;
        }

        protected EmployeePayloadBase(IReadOnlyList<UserError> errors)
            : base(errors)
        {
        }
    }
}
