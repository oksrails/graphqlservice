﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Types;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace GraphQLService.Api.Employees
{
    [ExtendObjectType("Query")]
    public class EmployeeQueries
    {
        [UseToDoDbCOntext]
        [UsePaging(typeof(NonNullType<EmployeeType>))]
        public IQueryable<EmployeeModel> GetEmployees([ScopedService] ToDoDbContext dbContext) =>
            dbContext.Employees;

        [UseToDoDbCOntext]
        public Task<EmployeeModel> GetEmployeeAsync(
            Guid id,
            EmployeeByIdDataLoader dataLoader,
            CancellationToken cancellationToken) =>
            dataLoader.LoadAsync(id, cancellationToken);

        [UseToDoDbCOntext]
        public async Task<IEnumerable<EmployeeModel>> GetEmployeesByIdAsync(
            Guid[] ids,
            EmployeeByIdDataLoader dataLoader,
            CancellationToken cancellationToken) =>
            await dataLoader.LoadAsync(ids, cancellationToken);
    }
}
