﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System.Threading.Tasks;
using GraphQLService.Api.Extensions;

namespace GraphQLService.Api.Employees
{
    [ExtendObjectType("Mutation")]
    public class EmployeeMutation
    {
        [UseToDoDbCOntext]
        public async Task<AddEmployeePayload> AddEmployeeAsync(
            AddEmployeeInput input,
            [ScopedService] ToDoDbContext dbContext)
        {
            var employee = new EmployeeModel
            {
                FirstName = input.FirstName,
                LastName = input.LastName
            };

            dbContext.Employees.Add(employee);
            await dbContext.SaveChangesAsync();

            return new AddEmployeePayload(employee);
        }
    }
}
