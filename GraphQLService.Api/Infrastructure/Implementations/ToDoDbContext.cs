﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Infrastructure.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace GraphQLService.Api.Infrastructure.Implementations
{
    public class ToDoDbContext : DbContext, IToDoDbContext
    {
        public DbSet<TaskModel> Tasks { get; set; } = default!;

        public DbSet<ProjectModel> Projects { get; set; } = default!;

        public DbSet<EmployeeModel> Employees { get; set; } = default!;

        public DbSet<TaskTypeModel> TaskTypes { get; set; } = default!;

        public DbSet<TimeAccountingModel> TimeAccountings { get; set; } = default!;

        public ToDoDbContext(DbContextOptions<ToDoDbContext> options)
            : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<ProjectEmployeesModel>()
                .HasKey(pe => new { pe.EmployeeId, pe.ProjectId });
        }
    }
}
