﻿using GraphQLService.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading.Tasks;
using System.Threading;

namespace GraphQLService.Api.Infrastructure.Abstractions
{
    public interface IToDoDbContext
    {
        DbSet<TaskModel> Tasks { get; set; }

        DatabaseFacade Database { get; }

        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
