﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using GraphQLService.Api.Types;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Tasks
{
    [ExtendObjectType("Query")]
    public class TaskQueries
    {
        [UseToDoDbCOntext]
        [UsePaging(typeof(NonNullType<TaskType>))]
        [UseFiltering(typeof(TaskFilterInputType))]
        [UseSorting]
        public IQueryable<TaskModel> GetTasks([ScopedService] ToDoDbContext context) =>
            context.Tasks;

        [UseToDoDbCOntext]
        public Task<TaskModel> GetTaskAsync(
            Guid id,
            TaskByIdDataLoader dataLoader,
            CancellationToken cancellationToken)
        {
            return dataLoader.LoadAsync(id, cancellationToken);
        }
    }
}
