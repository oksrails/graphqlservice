﻿using GraphQLService.Api.Common;
using GraphQLService.Api.Models;
using System.Collections.Generic;

namespace GraphQLService.Api.Tasks
{
    public class TaskPayloadBase : Payload
    {
        public TaskModel? Task { get; }

        protected TaskPayloadBase(TaskModel task)
        {
            Task = task;
        }

        protected TaskPayloadBase(IReadOnlyList<UserError> errors)
            : base(errors)
        {
        }
    }
}
