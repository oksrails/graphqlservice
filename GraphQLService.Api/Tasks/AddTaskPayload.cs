﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.Tasks
{
    public class AddTaskPayload : TaskPayloadBase
    {
        public AddTaskPayload(TaskModel task)
            : base(task)
        {
        }

        public AddTaskPayload(IReadOnlyList<UserError> errors)
            : base(errors)
        {
        }
    }
}
