﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.Tasks
{
    public class AssignTaskPayload : TaskPayloadBase
    {
        public AssignTaskPayload(TaskModel task)
            : base(task)
        { }

        public AssignTaskPayload(UserError error)
            : base(new UserError[] { error })
        { }

        public AssignTaskPayload(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
