﻿using System;

namespace GraphQLService.Api.Tasks
{
    public record AddTaskInput(
        string Title,
        string? Description,
        bool IsCompleted,
        Guid ProjectId,
        Guid TaskTypeId,
        Guid? AssignedId
    );
}
