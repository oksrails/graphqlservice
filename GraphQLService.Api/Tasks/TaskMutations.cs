﻿using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using GraphQLService.Api.Common;
using HotChocolate.Subscriptions;

namespace GraphQLService.Api.Tasks
{
    [ExtendObjectType("Mutation")]
    public class TaskMutations
    {
        [UseToDoDbCOntext]
        public async Task<AddTaskPayload> AddTaskAsync(
            AddTaskInput input,
            [ScopedService] ToDoDbContext dbContext)
        {
            var task = new TaskModel
            {
                Title = input.Title,
                Description = input.Description,
                IsCompleted = input.IsCompleted,
                ProjectId = input.ProjectId,
                TaskTypeId = input.TaskTypeId,
                AssignedId = input.AssignedId
            };

            dbContext.Tasks.Add(task);
            await dbContext.SaveChangesAsync();

            return new AddTaskPayload(task);
        }

        [UseToDoDbCOntext]
        public async Task<AssignTaskPayload> AssignTaskAsync(
            AssignTaskInput input,
            [ScopedService] ToDoDbContext dbContext,
            [Service] ITopicEventSender eventSender,
            CancellationToken cancellationToken)
        {
            TaskModel task = await dbContext.Tasks.FirstOrDefaultAsync(t => t.Id == input.TaskId, cancellationToken);

            if(!await dbContext.Employees.AnyAsync(e => e.Id == input.EmployeeId))
            {
                return new AssignTaskPayload(new UserError("Employee not found.", "EMPLOYEE_NOT_FOUND"));
            }

            if (task is null)
            {
                return new AssignTaskPayload(new UserError("Task not found.", "TASK_NOT_FOUND"));
            }

            task.AssignedId = input.EmployeeId;
            await dbContext.SaveChangesAsync();

            await eventSender.SendAsync(
                nameof(TaskSubscriptions.OnTaskAssignedAsync),
                input.TaskId,
                cancellationToken);

            return new AssignTaskPayload(task);
        }
    }
}
