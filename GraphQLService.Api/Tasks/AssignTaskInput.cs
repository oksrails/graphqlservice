﻿using System;

namespace GraphQLService.Api.Tasks
{
    public record AssignTaskInput(
        Guid TaskId,
        Guid EmployeeId
        );
}
