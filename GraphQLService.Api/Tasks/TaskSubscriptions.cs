﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Tasks
{
    [ExtendObjectType("Subscription")]
    public class TaskSubscriptions
    {
        public class AssignmentMessage 
        {
            public string? Message { get; set; }

            public TaskModel Task { get; set; }
        }

        [Subscribe]
        [Topic]
        public async Task<AssignmentMessage> OnTaskAssignedAsync(
            [EventMessage] Guid taskId,
            TaskByIdDataLoader dataLoader,
            CancellationToken cancellationToken)
        {
            TaskModel task = await dataLoader.LoadAsync(taskId, cancellationToken);

            return new AssignmentMessage
            {
                Message = $"Task '{task.Title}' was assigned on {task.Assigned.LastName} {task.Assigned.FirstName}",
                Task = task
            };
        }
    }
}
