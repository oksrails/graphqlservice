﻿namespace GraphQLService.Api.Common
{
    public class UserError
    {
        public string Message { get; set; }

        public string Code { get; set; }

        public UserError(string message, string code)
        {
            Message = message;
            Code = code;
        }
    }
}
