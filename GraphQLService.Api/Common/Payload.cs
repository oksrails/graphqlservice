﻿using System.Collections.Generic;

namespace GraphQLService.Api.Common
{
    public abstract class Payload
    {
        public IReadOnlyList<UserError> Errors { get; set; }

        protected Payload(IReadOnlyList<UserError>? errors = null)
        {
            Errors = errors;
        }
    }
}
