﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using GreenDonut;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.DataLoaders
{
    public class TaskTypeByIdDataLoader : BatchDataLoader<Guid, TaskTypeModel>
    {
        private readonly IDbContextFactory<ToDoDbContext> _dbContextFactory;

        public TaskTypeByIdDataLoader(
            IBatchScheduler batchScheduler,
            IDbContextFactory<ToDoDbContext> dbContextFactory)
            : base(batchScheduler)
        {
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
        }

        protected override async Task<IReadOnlyDictionary<Guid, TaskTypeModel>> LoadBatchAsync(
            IReadOnlyList<Guid> keys, 
            CancellationToken cancellationToken)
        {
            ToDoDbContext dbContext = _dbContextFactory.CreateDbContext();

            return await dbContext.TaskTypes
                .Where(t => keys.Contains(t.Id))
                .ToDictionaryAsync(d => d.Id, cancellationToken);
        }
    }
}
