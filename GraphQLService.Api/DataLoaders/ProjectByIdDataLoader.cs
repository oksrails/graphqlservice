﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using GreenDonut;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.DataLoaders
{
    public class ProjectByIdDataLoader : BatchDataLoader<Guid, ProjectModel>
    {
        private readonly IDbContextFactory<ToDoDbContext> _dbContextFactory;

        public ProjectByIdDataLoader(
            IBatchScheduler batchScheduler,
            IDbContextFactory<ToDoDbContext> dbContextFactory)
            : base(batchScheduler)
        {
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
        }

        protected override async Task<IReadOnlyDictionary<Guid, ProjectModel>> LoadBatchAsync(
            IReadOnlyList<Guid> keys, 
            CancellationToken cancellationToken)
        {
            ToDoDbContext dbContext = _dbContextFactory.CreateDbContext();

            var projects = await dbContext.Projects
                .Where(p => keys.Contains(p.Id))
                .ToDictionaryAsync(d => d.Id, cancellationToken);

            return projects;
        }
    }
}
