﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using GreenDonut;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.DataLoaders
{
    public class TaskByIdDataLoader : BatchDataLoader<Guid, TaskModel>
    {
        private readonly IDbContextFactory<ToDoDbContext> _dbContextFactory;

        public TaskByIdDataLoader(
            IBatchScheduler batchScheduler,
            IDbContextFactory<ToDoDbContext> dbContextFactory)
            : base(batchScheduler)
        {
            _dbContextFactory = dbContextFactory ?? throw new ArgumentNullException(nameof(dbContextFactory));
        }

        protected override async Task<IReadOnlyDictionary<Guid, TaskModel>> LoadBatchAsync(
            IReadOnlyList<Guid> keys, 
            CancellationToken cancellationToken)
        {
            ToDoDbContext dbContext = _dbContextFactory.CreateDbContext();

            return await dbContext.Tasks
                .Where(t => keys.Contains(t.Id))
                .Include(t => t.Assigned)
                .ToDictionaryAsync(d => d.Id, cancellationToken);
        }
    }
}
