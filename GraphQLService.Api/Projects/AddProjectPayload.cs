﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.Projects
{
    public class AddProjectPayload : ProjectPayloadBase
    {
        public AddProjectPayload(UserError error)
            : base(new[] { error })
        { }

        public AddProjectPayload(ProjectModel project)
            : base(project)
        { }

        public AddProjectPayload(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
