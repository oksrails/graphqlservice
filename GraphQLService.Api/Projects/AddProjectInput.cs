﻿using System;
using System.Collections.Generic;
using HotChocolate.Types.Relay;
using GraphQLService.Api.Models;

namespace GraphQLService.Api.Projects
{
    public record AddProjectInput(
        string Name,
        string Description,
        Guid OwnerId,
        [ID(nameof(EmployeeModel))]IReadOnlyList<Guid> EmployeeIds
        );
}
