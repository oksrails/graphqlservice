﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using GraphQLService.Api.Types;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GraphQLService.Api.Projects
{
    [ExtendObjectType("Query")]
    public class ProjectQueries
    {

        [UseToDoDbCOntext]
        [UsePaging(typeof(NonNullType<ProjectType>))]
        [UseFiltering(typeof(ProjectFilterInputType))]
        [UseSorting]
        public IQueryable<ProjectModel> GetProjects([ScopedService] ToDoDbContext dbContext) =>
            dbContext.Projects;

        [UseToDoDbCOntext]
        public Task<ProjectModel> GetProjectByIdAsync(
            Guid id,
            ProjectByIdDataLoader dataLoader,
            CancellationToken cancellationToken) =>
            dataLoader.LoadAsync(id, cancellationToken);

        [UseToDoDbCOntext]
        public async Task<IEnumerable<ProjectModel>> GetProjectsByIdAsync(
            Guid[] ids,
            ProjectByIdDataLoader dataLoader,
            CancellationToken cancellationToken) =>
            await dataLoader.LoadAsync(ids, cancellationToken);
    }
}
