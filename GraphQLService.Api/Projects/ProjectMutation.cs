﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System.Threading.Tasks;
using GraphQLService.Api.Extensions;
using System;

namespace GraphQLService.Api.Projects
{
    [ExtendObjectType("Mutation")]
    public class ProjectMutation
    {
        [UseToDoDbCOntext]
        public async Task<AddProjectPayload> AddProjectAsync(
            AddProjectInput input,
            [ScopedService] ToDoDbContext dbContext)
        {
            var project = new ProjectModel
            {
                Name = input.Name,
                Description = input.Description,
                OwnerId = input.OwnerId
            };

            foreach (Guid employeeId in input.EmployeeIds)
            {
                project.ProjectEmployees.Add(new ProjectEmployeesModel
                {
                    EmployeeId = employeeId
                });
            }

            dbContext.Projects.Add(project);
            await dbContext.SaveChangesAsync();

            return new AddProjectPayload(project);
        }
    }
}
