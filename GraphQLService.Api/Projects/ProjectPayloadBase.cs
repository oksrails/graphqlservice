﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.Projects
{
    public class ProjectPayloadBase : Payload
    {
        public ProjectModel Project { get; }

        protected ProjectPayloadBase(ProjectModel project)
        {
            Project = project;
        }

        protected ProjectPayloadBase(IReadOnlyList<UserError> errors)
            : base(errors) 
        { }
    }
}
