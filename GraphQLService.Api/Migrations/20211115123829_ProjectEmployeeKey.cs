﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GraphQLService.Api.Migrations
{
    public partial class ProjectEmployeeKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_projects_employees_owner_id",
                table: "projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_project_employees",
                table: "project_employees");

            migrationBuilder.DropIndex(
                name: "IX_project_employees_employee_id",
                table: "project_employees");

            migrationBuilder.AlterColumn<Guid>(
                name: "owner_id",
                table: "projects",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_project_employees",
                table: "project_employees",
                columns: new[] { "employee_id", "project_id" });

            migrationBuilder.AddForeignKey(
                name: "FK_projects_employees_owner_id",
                table: "projects",
                column: "owner_id",
                principalTable: "employees",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_projects_employees_owner_id",
                table: "projects");

            migrationBuilder.DropPrimaryKey(
                name: "PK_project_employees",
                table: "project_employees");

            migrationBuilder.AlterColumn<Guid>(
                name: "owner_id",
                table: "projects",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_project_employees",
                table: "project_employees",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_project_employees_employee_id",
                table: "project_employees",
                column: "employee_id");

            migrationBuilder.AddForeignKey(
                name: "FK_projects_employees_owner_id",
                table: "projects",
                column: "owner_id",
                principalTable: "employees",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
