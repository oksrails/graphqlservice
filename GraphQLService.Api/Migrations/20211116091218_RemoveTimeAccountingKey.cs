﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace GraphQLService.Api.Migrations
{
    public partial class RemoveTimeAccountingKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_time_accounting",
                table: "time_accounting");

            migrationBuilder.AddPrimaryKey(
                name: "PK_time_accounting",
                table: "time_accounting",
                column: "id");

            migrationBuilder.CreateIndex(
                name: "IX_time_accounting_task_id",
                table: "time_accounting",
                column: "task_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_time_accounting",
                table: "time_accounting");

            migrationBuilder.DropIndex(
                name: "IX_time_accounting_task_id",
                table: "time_accounting");

            migrationBuilder.AddPrimaryKey(
                name: "PK_time_accounting",
                table: "time_accounting",
                columns: new[] { "task_id", "employee_id" });
        }
    }
}
