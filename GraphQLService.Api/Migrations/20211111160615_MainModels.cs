﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GraphQLService.Api.Migrations
{
    public partial class MainModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "tasks",
                newName: "id");

            migrationBuilder.AddColumn<Guid>(
                name: "assigned_id",
                table: "tasks",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "task_type_id",
                table: "tasks",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "employees",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    first_name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    last_name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employees", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "task_types",
                columns: table => new
                {
                    id = table.Column<Guid>(type: "uuid", nullable: false),
                    name = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_task_types", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tasks_assigned_id",
                table: "tasks",
                column: "assigned_id");

            migrationBuilder.CreateIndex(
                name: "IX_tasks_task_type_id",
                table: "tasks",
                column: "task_type_id");

            migrationBuilder.AddForeignKey(
                name: "FK_tasks_employees_assigned_id",
                table: "tasks",
                column: "assigned_id",
                principalTable: "employees",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_tasks_task_types_task_type_id",
                table: "tasks",
                column: "task_type_id",
                principalTable: "task_types",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tasks_employees_assigned_id",
                table: "tasks");

            migrationBuilder.DropForeignKey(
                name: "FK_tasks_task_types_task_type_id",
                table: "tasks");

            migrationBuilder.DropTable(
                name: "employees");

            migrationBuilder.DropTable(
                name: "task_types");

            migrationBuilder.DropIndex(
                name: "IX_tasks_assigned_id",
                table: "tasks");

            migrationBuilder.DropIndex(
                name: "IX_tasks_task_type_id",
                table: "tasks");

            migrationBuilder.DropColumn(
                name: "assigned_id",
                table: "tasks");

            migrationBuilder.DropColumn(
                name: "task_type_id",
                table: "tasks");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "tasks",
                newName: "Id");
        }
    }
}
