﻿using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using GraphQLService.Api.Types;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using System.Linq;

namespace GraphQLService.Api.TimeAccounting
{
    [ExtendObjectType("Query")]
    public class TimeAccountingQueries
    {
        [UseToDoDbCOntext]
        [UsePaging(typeof(NonNullType<TimeAccountinType>))]
        [UseFiltering(typeof(TimeAccountingFilterInputType))]
        [UseSorting]
        public IQueryable<TimeAccountingModel> GetTimeAccountings([ScopedService] ToDoDbContext dbContext) =>
            dbContext.TimeAccountings.OrderBy(t => t.Date);
    }
}
