﻿using System;

namespace GraphQLService.Api.TimeAccounting
{
    public record AddTimeAccountingInput(
        Guid EmployeeId,
        Guid TaskId,
        DateTime Date,
        float HoursSpent,
        string? Description
    );
}
