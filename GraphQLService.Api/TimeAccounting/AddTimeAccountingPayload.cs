﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.TimeAccounting
{
    public class AddTimeAccountingPayload: TimeAccountingPayloadBase
    {
        public AddTimeAccountingPayload(TimeAccountingModel timeAccounting)
            : base(timeAccounting)
        { }

        public AddTimeAccountingPayload(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
