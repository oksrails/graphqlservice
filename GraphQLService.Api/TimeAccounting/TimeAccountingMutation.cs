﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System.Threading.Tasks;
using GraphQLService.Api.Extensions;
using System;

namespace GraphQLService.Api.TimeAccounting
{
    [ExtendObjectType("Mutation")]
    public class TimeAccountingMutation
    {
        [UseToDoDbCOntext]
        public async Task<AddTimeAccountingPayload> AddTimeAccountingAsync(
            AddTimeAccountingInput input,
            [ScopedService] ToDoDbContext dbContext)
        {
            var timeAccounting = new TimeAccountingModel
            {
                EmployeeId = input.EmployeeId,
                TaskId = input.TaskId,
                Date = DateTime.Parse(input.Date.ToString()),
                HoursSpent = input.HoursSpent,
                Description = input.Description
            };

            dbContext.TimeAccountings.Add(timeAccounting);
            await dbContext.SaveChangesAsync();

            return new AddTimeAccountingPayload(timeAccounting);
        }
    }
}
