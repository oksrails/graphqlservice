﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.TimeAccounting
{
    public class TimeAccountingPayloadBase : Payload
    {
        public TimeAccountingModel TimeAccounting { get; }

        protected TimeAccountingPayloadBase(TimeAccountingModel timeAccounting)
        {
            TimeAccounting = timeAccounting;
        }

        protected TimeAccountingPayloadBase(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
