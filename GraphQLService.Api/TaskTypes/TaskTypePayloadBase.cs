﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.TaskTypes
{
    public class TaskTypePayloadBase : Payload
    {
        public TaskTypeModel TaskType { get; }

        protected TaskTypePayloadBase(TaskTypeModel taskType)
        {
            TaskType = taskType;
        }

        protected TaskTypePayloadBase(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
