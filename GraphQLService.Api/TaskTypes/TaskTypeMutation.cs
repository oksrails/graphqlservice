﻿using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using System.Threading.Tasks;
using GraphQLService.Api.Extensions;
using System;

namespace GraphQLService.Api.TaskTypes
{
    [ExtendObjectType("Mutation")]
    public class TaskTypeMutation
    {
        [UseToDoDbCOntext]
        public async Task<AddTaskTypePayload> AddTaskTypeAsync(
            AddTaskTypeInput input,
            [ScopedService] ToDoDbContext dbContext)
        {
            var taskType = new TaskTypeModel
            {
                Name = input.Name
            };

            dbContext.TaskTypes.Add(taskType);
            await dbContext.SaveChangesAsync();

            return new AddTaskTypePayload(taskType);
        }
    }
}
