﻿using GraphQLService.Api.Models;
using GraphQLService.Api.Common;
using System.Collections.Generic;

namespace GraphQLService.Api.TaskTypes
{
    public class AddTaskTypePayload : TaskTypePayloadBase
    {
        public AddTaskTypePayload(UserError error)
            : base(new[] { error })
        { }

        public AddTaskTypePayload(TaskTypeModel taskType)
            : base(taskType)
        { }

        public AddTaskTypePayload(IReadOnlyList<UserError> errors)
            : base(errors)
        { }
    }
}
