﻿using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Extensions;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Models;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GraphQLService.Api.TaskTypes
{
    [ExtendObjectType("Query")]
    public class TaskTypeQueries
    {
        [UseToDoDbCOntext]
        [UsePaging]
        public IQueryable<TaskTypeModel> GetTaskTypes([ScopedService] ToDoDbContext context) =>
            context.TaskTypes.OrderBy(t => t.Name);

        [UseToDoDbCOntext]
        public Task<TaskTypeModel> GetTaskTypeAsync(
            Guid id,
            TaskTypeByIdDataLoader dataLoader,
            CancellationToken cancellationToken)
        {
            return dataLoader.LoadAsync(id, cancellationToken);
        }
    }
}
