﻿namespace GraphQLService.Api.TaskTypes
{
    public record AddTaskTypeInput(
        string Name
        );
}
