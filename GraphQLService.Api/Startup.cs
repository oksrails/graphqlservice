using GraphQLService.Api.DataLoaders;
using GraphQLService.Api.Infrastructure.Implementations;
using GraphQLService.Api.Tasks;
using GraphQLService.Api.Employees;
using GraphQLService.Api.Projects;
using GraphQLService.Api.Types;
using GraphQLService.Api.TaskTypes;
using GraphQLService.Api.TimeAccounting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Identity.Web;
using Microsoft.OpenApi.Models;

namespace GraphQLService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddMicrosoftIdentityWebApi(Configuration.GetSection("AzureAd"));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "GraphQLService.Api", Version = "v1" });
            });

            services.AddPooledDbContextFactory<ToDoDbContext>(options => 
                options.UseNpgsql(Configuration.GetConnectionString("PostgresConnectionString")));

            services
                .AddGraphQLServer()
                .AddQueryType(q => q.Name("Query"))
                    .AddTypeExtension<TaskQueries>()
                    .AddTypeExtension<TaskTypeQueries>()
                    .AddTypeExtension<EmployeeQueries>()
                    .AddTypeExtension<ProjectQueries>()
                    .AddTypeExtension<TimeAccountingQueries>()
                .AddMutationType(m => m.Name("Mutation"))
                    .AddTypeExtension<TaskMutations>()
                    .AddTypeExtension<TaskTypeMutation>()
                    .AddTypeExtension<EmployeeMutation>()
                    .AddTypeExtension<ProjectMutation>()
                    .AddTypeExtension<TimeAccountingMutation>()
                .AddSubscriptionType(s => s.Name("Subscription"))
                    .AddTypeExtension<TaskSubscriptions>()
                .AddType<EmployeeType>()
                .AddType<ProjectType>()
                .AddType<TaskTypeType>()
                .AddGlobalObjectIdentification()
                .AddFiltering()
                .AddSorting()
                .AddInMemorySubscriptions()
                .AddDataLoader<TaskByIdDataLoader>()
                .AddDataLoader<EmployeeByIdDataLoader>()
                .AddDataLoader<ProjectByIdDataLoader>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "GraphQLService.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseWebSockets();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGraphQL();
            });

            MigrateDatabase(app);
        }

        private void MigrateDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<IDbContextFactory<ToDoDbContext>>().CreateDbContext().Database.Migrate();
            }
        }
    }
}
