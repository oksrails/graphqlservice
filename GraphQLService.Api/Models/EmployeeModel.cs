﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace GraphQLService.Api.Models
{
    [Table("employees")]
    public class EmployeeModel
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("first_name")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Column("last_name")]
        [StringLength(100)]
        public string LastName { get; set; }

        public ICollection<TaskModel> Tasks { get; set; } = new List<TaskModel>();

        public ICollection<TimeAccountingModel> TimeAccountings { get; set; } = new List<TimeAccountingModel>();

        public ICollection<ProjectEmployeesModel> ProjectEmployees { get; set; } = new List<ProjectEmployeesModel>();
    }
}
