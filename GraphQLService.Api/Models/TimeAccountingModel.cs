﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphQLService.Api.Models
{
    [Table("time_accounting")]
    public class TimeAccountingModel
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("employee_id")]
        public Guid EmployeeId { get; set; }

        public EmployeeModel Employee { get; set; }

        [Column("task_id")]
        public Guid TaskId { get; set; }

        public TaskModel Task { get; set; }

        [Column("date")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Column("hours_spent")]
        public float HoursSpent { get; set; }

        [Column("description")]
        [StringLength(4000)]
        public string Description { get; set; }
    }
}
