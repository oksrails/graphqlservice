﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphQLService.Api.Models
{
    [Table("task_types")]
    public class TaskTypeModel
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("name")]
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public ICollection<TaskModel> Tasks { get; set; } = new List<TaskModel>();
    }
}
