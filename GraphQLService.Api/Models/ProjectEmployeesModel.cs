﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphQLService.Api.Models
{
    [Table("project_employees")]
    public class ProjectEmployeesModel
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("project_id")]
        public Guid ProjectId { get; set; }

        public ProjectModel? Project { get; set; }

        [Column("employee_id")]
        public Guid EmployeeId { get; set; }

        public EmployeeModel? Employee { get; set; }
    }
}
