﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphQLService.Api.Models
{
    /// <summary>
    /// Describe task entity
    /// </summary>
    [Table("tasks")]
    public class TaskModel
    {
        /// <summary>
        /// Task identifier
        /// </summary>
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Task title
        /// </summary>
        [Required]
        [Column("title")]
        [StringLength(200)]
        public string Title { get; set; }

        /// <summary>
        /// Task description
        /// </summary>
        [Column("description")]
        [StringLength(10000)]
        public string? Description { get; set; }

        /// <summary>
        /// Task complition status
        /// </summary>
        [Column("isComplited")]
        public bool IsCompleted { get; set; }

        [Column("project_id")]
        public Guid? ProjectId { get; set; }

        public ProjectModel? Project { get; set; }

        [Column("task_type_id")]
        public Guid? TaskTypeId { get; set; }

        public TaskTypeModel? TaskType { get; set; }

        [Column("assigned_id")]
        public Guid? AssignedId { get; set; }

        public EmployeeModel? Assigned { get; set; }

        public ICollection<TimeAccountingModel> TimeAccountings { get; set; } = new List<TimeAccountingModel>();
    }
}
