﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GraphQLService.Api.Models
{
    [Table("projects")]
    public class ProjectModel
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("name")]
        [StringLength(200)]
        public string Name { get; set; }

        [Column("description")]
        [StringLength(4000)]
        public string Description { get; set; }


        [Column("owner_id")]
        public Guid? OwnerId { get; set; }

        public EmployeeModel? Owner { get; set; }

        public ICollection<TaskModel>? Tasks { get; set; } = new List<TaskModel>();

        public ICollection<ProjectEmployeesModel> ProjectEmployees { get; set; } = new List<ProjectEmployeesModel>();
    }
}