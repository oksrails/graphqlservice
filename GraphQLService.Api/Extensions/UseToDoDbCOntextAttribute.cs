﻿using System.Reflection;
using GraphQLService.Api.Infrastructure.Implementations;
using HotChocolate.Types;
using HotChocolate.Types.Descriptors;

namespace GraphQLService.Api.Extensions
{
    public class UseToDoDbCOntextAttribute : ObjectFieldDescriptorAttribute
    {
        public override void OnConfigure(
            IDescriptorContext context, 
            IObjectFieldDescriptor descriptor, 
            MemberInfo member)
        {
            descriptor.UseDbContext<ToDoDbContext>();
        }
    }
}
